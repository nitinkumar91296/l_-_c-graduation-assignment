package main

import (
	"bufio"
	"fmt"
	"learnCode/l_-_c-graduation-assignment/config"
	"log"
	"net"
	"os"
	"strings"
)

func main() {
	conn, err := dialConnection()
	if err != nil {
		log.Fatalf("failed to initialize the client connection : %v", err)
	}

	for {
		fmt.Println("Send Message : ")
		reader := bufio.NewReader(os.Stdin)
		text, err := reader.ReadString('\n')
		if err != nil {
			log.Println("failed to read text message")
		}

		fmt.Fprintf(conn, text+"\n")

		message, _ := bufio.NewReader(conn).ReadString('\n')
		fmt.Print("Received Message: " + message)
		if strings.TrimSpace(string(text)) == "STOP" {
			fmt.Println("client stopping..... !!")
			conn.Close()
			return
		}
	}
}

func dialConnection() (net.Conn, error) {
	conn, err := net.Dial("tcp", config.ApplicationPort)
	if err != nil {
		log.Fatalf("failed to initialize the client connection : %v", err)
		return nil, err
	}
	return conn, nil
}
