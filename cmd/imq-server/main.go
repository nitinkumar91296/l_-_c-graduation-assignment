package main

import (
	"fmt"
	"learnCode/l_-_c-graduation-assignment/cmd/imq-server/handler"
	"learnCode/l_-_c-graduation-assignment/config"
	mysql "learnCode/l_-_c-graduation-assignment/internal/storage"
	"net"

	"github.com/sirupsen/logrus"
)

var log = logrus.New()

func main() {

	db, err := createDSN()
	if err != nil {
		log.Error(err)
	}

	lis, err := connectionListener()
	if err != nil {
		log.Errorf("failed to initialize listener: %v", err.Error())
	}
	defer lis.Close()

	log.Print("Server is Ready to serve on port %s ", config.ApplicationPort)
	for {
		conn, err := lis.Accept()
		if err != nil {
			log.Errorf("error while accepting connection: %v", err.Error())
		}
		defer conn.Close()
		go handler.ServerHandler(conn, db)
	}
}

func createDSN() (mysql.DatabaseIF, error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", config.DbUserName, config.DbPassword, config.DbHost, config.DbPort, config.DbName)
	db, err := mysql.NewSQLDB(dsn, log)
	if err != nil {
		log.Error(err)
	}
	return db, nil
}

func connectionListener() (net.Listener, error) {
	lis, err := net.Listen("tcp", config.ApplicationPort)
	if err != nil {
		log.Errorf("failed to initialize listener: %v", err.Error())
	}
	return lis, nil
}
