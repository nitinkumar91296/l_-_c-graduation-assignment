package handler

import (
	"bufio"
	"fmt"
	"learnCode/l_-_c-graduation-assignment/internal/domain"
	mysql "learnCode/l_-_c-graduation-assignment/internal/storage"
	"log"
	"net"
	"strings"
)

// ServerHandler handles server connections
func ServerHandler(conn net.Conn, db mysql.DatabaseIF) {
	for {

		data, err := bufio.NewReader(conn).ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		if strings.TrimSpace(data) == "STOP" {
			fmt.Print("Stopped\n")
			conn.Close()
			return
		}

		resp, err := domain.ProcessMessage(db, data)

		msg := fmt.Sprintf("[%s: %s]: %s\n", resp.Name, resp.Timestamp, resp.Data)
		if err != nil {
			msg = fmt.Sprintf("[%s: %s]: Error: %s\n", resp.Name, resp.Timestamp, err.Error())
		}

		fmt.Printf("%s", msg)
		conn.Write([]byte(msg))
	}
}
