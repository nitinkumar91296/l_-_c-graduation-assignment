SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imqueue`
--

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `id` int(11) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `data` varchar(1024) NOT NULL,
  `clientId` int(11) DEFAULT NULL,
  `topicId` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `expiredAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `publishertopicmap`
--

CREATE TABLE `publishertopicmap` (
  `id` int(11) NOT NULL,
  `clientId` int(11) NOT NULL,
  `topicId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `subscribertopicmap`
--

CREATE TABLE `subscribertopicmap` (
  `id` int(11) NOT NULL,
  `clientId` int(11) NOT NULL,
  `topicId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`,`password`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clientId` (`clientId`),
  ADD KEY `messages_ibfk_2` (`topicId`);

--
-- Indexes for table `publishertopicmap`
--
ALTER TABLE `publishertopicmap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clientId` (`clientId`),
  ADD KEY `topicId` (`topicId`);

--
-- Indexes for table `subscribertopicmap`
--
ALTER TABLE `subscribertopicmap`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clientId` (`clientId`),
  ADD KEY `topicId` (`topicId`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `subscribertopicmap`
--
ALTER TABLE `subscribertopicmap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `messages_ibfk_1` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `messages_ibfk_2` FOREIGN KEY (`topicId`) REFERENCES `topic` (`id`);

--
-- Constraints for table `publishertopicmap`
--
ALTER TABLE `publishertopicmap`
  ADD CONSTRAINT `publishertopicmap_ibfk_1` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `publishertopicmap_ibfk_2` FOREIGN KEY (`topicId`) REFERENCES `topic` (`id`);

--
-- Constraints for table `subscribertopicmap`
--
ALTER TABLE `subscribertopicmap`
  ADD CONSTRAINT `subscribertopicmap_ibfk_1` FOREIGN KEY (`clientId`) REFERENCES `client` (`id`),
  ADD CONSTRAINT `subscribertopicmap_ibfk_2` FOREIGN KEY (`topicId`) REFERENCES `topic` (`id`);
COMMIT;
