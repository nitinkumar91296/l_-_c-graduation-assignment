module learnCode/l_-_c-graduation-assignment

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/konsorten/go-windows-terminal-sequences v1.0.3 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.7.0
)
