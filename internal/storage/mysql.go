package mysql

import (
	"database/sql"

	_ "github.com/go-sql-driver/mysql"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var mySQLDriver = "mysql"

// DatabaseIF interface conatains the DB related functions
type DatabaseIF interface {
	Connect() error
	Test() error
	CreateTable(name string) error
	SaveMessages(client string, cd ClientData) error
	SearchClient(name string) (bool, error)
	StoreClient(clientName string) error
}

// DB struct contains the connection relates values
type DB struct {
	Dsn string
	Cxn *sql.DB
	Log *logrus.Logger
}

// NewSQLDB is the factory to return new DatabaseIF for db
func NewSQLDB(dsn string, log *logrus.Logger) (DatabaseIF, error) {
	mysql := &DB{
		Dsn: dsn,
		Log: log,
	}

	err := mysql.Connect()
	if err != nil {
		return nil, err
	}

	return mysql, nil

}

// Connect function connects to the db
func (mysql *DB) Connect() error {
	cxn, err := sql.Open(mySQLDriver, mysql.Dsn)
	if err != nil {
		return errors.Wrap(err, "unable to create connection to db")
	}
	mysql.Cxn = cxn
	if err := mysql.Test(); err != nil {
		return err
	}

	return nil
}

// Test ping the db to check the connectivity
func (mysql *DB) Test() error {

	if err := mysql.Cxn.Ping(); err != nil {
		return errors.Wrap(err, "unable to ping db")
	}
	return nil
}

// CreateTable creates table
func (mysql *DB) CreateTable(name string) error {

	stmt, err := mysql.Cxn.Prepare("CREATE TABLE `" + name + "` (`serial_no` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY, `timestamp` timestamp NULL DEFAULT NULL, `data` varchar(500) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4")
	if err != nil {
		return err
	}

	_, err = stmt.Exec()
	if err != nil {
		return err
	}

	return nil
}

// StoreClient store client details
func (mysql *DB) StoreClient(clientName string) error {

	stmt, err := mysql.Cxn.Prepare(`INSERT INTO Clients (client_name) VALUES(?)`)
	if err != nil {
		return err
	}

	_, err = stmt.Exec(clientName)
	if err != nil {
		return err
	}

	return nil
}

// SearchClient search a particular client in db
func (mysql *DB) SearchClient(name string) (bool, error) {

	var clientName string

	stmt := `SELECT client_name FROM Clients where client_name = ?`
	err := mysql.Cxn.QueryRow(stmt, name).Scan(&clientName)
	if err == sql.ErrNoRows {
		return false, nil
	}

	if err != nil {
		return false, err
	}

	return true, nil
}

// SaveMessages saves the message in db
func (mysql *DB) SaveMessages(client string, cd ClientData) error {

	stmt, err := mysql.Cxn.Prepare("INSERT INTO " + client + " (timestamp, data) VALUES(?,?)")
	if err != nil {
		return err
	}

	_, err = stmt.Exec(cd.Timestamp, cd.Data)
	if err != nil {
		return err
	}

	return nil
}
