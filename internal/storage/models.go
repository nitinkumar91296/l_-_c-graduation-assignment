package mysql

// Clients contains the client name
type Clients struct {
	ClientName string
}

// ClientData contains the data to store from client
type ClientData struct {
	ID        int
	Timestamp string
	Data      string
}
