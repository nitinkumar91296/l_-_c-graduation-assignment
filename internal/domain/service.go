package domain

import (
	mysql "learnCode/l_-_c-graduation-assignment/internal/storage"
	"strings"
	"time"

	"errors"
)

func ProcessMessage(db mysql.DatabaseIF, message string) (*ClientResponse, error) {
	var cd mysql.ClientData

	cr := &ClientResponse{}

	message = strings.TrimSpace(message)
	data := strings.Split(message, ":")

	if len(data) != 2 {
		return cr, errors.New("invalid message format")
	}

	cd.Data = data[1]
	cd.Timestamp = time.Now().Format("2006-01-02 15:04:05")

	cr.Name = data[0]
	cr.Timestamp = cd.Timestamp
	cr.Data = cd.Data

	isFound, err := db.SearchClient(data[0])
	if err != nil {
		return cr, err
	}

	if !isFound {
		if err := db.CreateTable(data[0]); err != nil {
			return cr, err
		}
		if err := db.StoreClient(data[0]); err != nil {
			return cr, err
		}
	}
	if err := db.SaveMessages(data[0], cd); err != nil {
		return cr, nil
	}

	return cr, nil

}
