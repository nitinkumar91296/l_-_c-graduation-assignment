package domain

// ClientResponse is the response for client
type ClientResponse struct {
	Name      string
	Timestamp string
	Data      string
}
