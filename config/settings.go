package config

var (
	DbUserName      = "root"
	DbPassword      = "password"
	DbHost          = "127.0.0.1"
	DbPort          = 3308
	DbName          = "messagingQueue"
	ApplicationPort = ":8080"
)
